package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.endpoint.Session;

public interface ISessionService {

    @Nullable
    Session getSession();

    void setSession(@Nullable Session session);

}
