package ru.tsc.gavran.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractTaskCommand;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.endpoint.Status;
import ru.tsc.gavran.tm.endpoint.Task;
import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-change-status-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change task status by id.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        final String userId = session.getUserId();
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        @Nullable final Task task = serviceLocator.getTaskEndpoint().changeTaskStatusById(session, id, status);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskEndpoint().changeTaskStatusById(session, id, status);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}