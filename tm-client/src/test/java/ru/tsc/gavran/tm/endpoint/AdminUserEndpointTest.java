package ru.tsc.gavran.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.endpoint.*;
import ru.tsc.gavran.tm.marker.SoapCategory;

import javax.xml.ws.WebServiceException;
import java.util.List;

public class AdminUserEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @Nullable
    private static Session session;

    @NotNull
    protected static final Bootstrap bootstrap = new Bootstrap();

    @BeforeClass
    public static void beforeClass() {
        bootstrap.getUserEndpoint().registryUser("Test1", "Test1", "Test@email.com");
        bootstrap.getUserEndpoint().registryUser("Admin", "Admin", "Test@email.com");
        session = bootstrap.getSessionEndpoint().openSession("Test1", "Test1");
    }

    @Before
    public void before() {
//        session = bootstrap.getSessionEndpoint().openSession("Test1", "Test1");
    }

    @After
    public void after() {
        bootstrap.getTaskEndpoint().clearTask(session);
        bootstrap.getProjectEndpoint().clearProject(session);
    }

    @AfterClass
    public static void afterClass() throws Exception {

    }

    @Test
    @Category(SoapCategory.class)
    public void findAllUser() {
        @NotNull final List<User> list = adminUserEndpoint.findAllUser(session);
        Assert.assertNotNull(list);
    }

    @Test
    @Category(SoapCategory.class)
    public void findUserById() {
        @NotNull final User user = adminUserEndpoint.findUserById(session, session.getUserId());
        Assert.assertNotNull(user);
        Assert.assertEquals(session.getUserId(), user.getId());
    }

    @Test
    @Category(SoapCategory.class)
    public void findUserByLogin() {
        @NotNull final User user = adminUserEndpoint.findUserByLogin(session, "Admin");
        Assert.assertNotNull(user);
        Assert.assertEquals("Admin", user.getLogin());
    }

    @Test
    @Category(SoapCategory.class)
    public void getCurrentUser() {
        @NotNull final User user = adminUserEndpoint.getCurrentUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(session.getUserId(), user.getId());
        Assert.assertEquals("Admin", user.getLogin());
    }

    @Test
    @Category(SoapCategory.class)
    public void lockUserByLogin() {
        adminUserEndpoint.lockUserByLogin(session, "User");
        @NotNull final User user = adminUserEndpoint.findUserByLogin(session, "User");
        Assert.assertNotNull(user);
        Assert.assertTrue(user.isLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void unlockByLogin() {
        adminUserEndpoint.unlockUserByLogin(session, "User");
        @NotNull final User user = adminUserEndpoint.findUserByLogin(session, "User");
        Assert.assertNotNull(user);
        Assert.assertFalse(user.isLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUserPassword() {
        adminUserEndpoint.updateUserPassword(session, "AdminNew");
        sessionEndpoint.closeSession(session);
        @NotNull final Session sessionNew = sessionEndpoint.openSession("Admin", "AdminNew");
        Assert.assertNotNull(sessionNew);
        adminUserEndpoint.updateUserPassword(sessionNew, "Admin");
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUserById() {
        @NotNull final User user = adminUserEndpoint.updateUserById(session, "FirstNameNew", "LastNameNew", "MiddleNameNew", "EmailNew123");
        Assert.assertNotNull(user);
        Assert.assertEquals("FirstNameNew", user.getFirstName());
        Assert.assertEquals("LastNameNew", user.getLastName());
        Assert.assertEquals("MiddleNameNew", user.getMiddleName());
        Assert.assertEquals("EmailNew123", user.getEmail());
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUserByLogin() {
        @NotNull final User user = adminUserEndpoint.updateUserByLogin(session, "Admin", "FirstNameNew2", "LastNameNew2", "MiddleNameNew2", "EmailNewTest12");
        Assert.assertNotNull(user);
        Assert.assertEquals("FirstNameNew2", user.getFirstName());
        Assert.assertEquals("LastNameNew2", user.getLastName());
        Assert.assertEquals("MiddleNameNew2", user.getMiddleName());
        Assert.assertEquals("EmailNewTest12", user.getEmail());
    }

    @Test
    @Category(SoapCategory.class)
    public void setUserRole() {
        @NotNull final User userTest = adminUserEndpoint.findUserByLogin(session, "User");
        @NotNull final User user = adminUserEndpoint.setUserRole(session, Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(Role.ADMIN, user.getRole());
        adminUserEndpoint.setUserRole(session, Role.USER);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void removeUserById() {
        @NotNull final User userTest = adminUserEndpoint.findUserByLogin(session, "Test");
        adminUserEndpoint.removeUserById(session, userTest.getId());
        session = sessionEndpoint.openSession("Test", "Test");
        Assert.assertNull(session);
        userEndpoint.registryUser("Test", "Test", "Test@email.ru");
    }

}