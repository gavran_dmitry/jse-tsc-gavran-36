package ru.tsc.gavran.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.endpoint.Project;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.endpoint.Status;
import ru.tsc.gavran.tm.endpoint.Task;
import ru.tsc.gavran.tm.marker.SoapCategory;

import javax.swing.*;
import java.util.Collection;
import java.util.List;

public class TaskEndpointTest {

    @NotNull
    protected static final Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Task task;

    @Nullable
    private Project project;

    @Nullable
    private static Session session;

    @BeforeClass
    public static void beforeClass() {
        bootstrap.getUserEndpoint().registryUser("Admin4", "Admin4", "Zhopa@mai4.ru");
        session = bootstrap.getSessionEndpoint().openSession("Admin4", "Admin4");
        bootstrap.getTaskEndpoint().clearTask(session);
        bootstrap.getProjectEndpoint().clearProject(session);
    }

    @Before
    public void before() {
        bootstrap.getTaskEndpoint().createTask(session, "TaskName1", "TaskDescription1");
        bootstrap.getProjectEndpoint().createProject(session, "ProjectName1", "ProjectDescription!");
        this.task = bootstrap.getTaskEndpoint().findTaskByName(session, "TaskName1");
        this.project = bootstrap.getProjectEndpoint().findProjectByName(session, "ProjectName1");
    }

    @After
    public void after() {
        bootstrap.getTaskEndpoint().clearTask(session);
        bootstrap.getProjectEndpoint().clearProject(session);
        bootstrap.getSessionEndpoint().closeSession(session);
    }

    @AfterClass
    public static void afterClass() {
        bootstrap.getSessionEndpoint().closeSession(session);
    }


    @Test
    @Category(SoapCategory.class)
    public void updateTaskById() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().updateTaskById(
                session,
                task.getId(),
                "NewTaskName",
                "NewTaskDesc"
        );
        Assert.assertNotNull(task1);
    }

    @Test
    public void finishTaskById() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().finishTaskById(session, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(Status.COMPLETED, task1.getStatus());
    }

    @Test
    @Ignore
    @Category(SoapCategory.class)
    public void findTaskByProjectId() {
        bootstrap.getTaskEndpoint().bindTaskById(session, project.getId(), task.getId());
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().findTaskById(session, task.getId());
        Assert.assertNotNull(task1.getProjectId());
        @Nullable final Collection<Task> list = bootstrap.getTaskEndpoint().findTaskByProjectId(session,task1.getProjectId());
        Assert.assertTrue(list.size() > 0);
    }

    @Test
    @Ignore
    @Category(SoapCategory.class)
    public void updateTaskByIndex() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().updateTaskByIndex(
                session,
                0,
                "TaskName2",
                "TaskDescription2");
        Assert.assertNotNull(task1);
        Assert.assertEquals("TaskName2", task1.getName());
        Assert.assertEquals("TaskDescription2", task1.getDescription());
    }

    @Test
    public void findTaskByIndex() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().findTaskByIndex(session, 0);
        Assert.assertNotNull(task1);
    }

    @Test
    public void startTaskById() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().startTaskById(session, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(Status.IN_PROGRESS, task1.getStatus());
    }

    @Test
    public void startTaskByName() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().startTaskByName(session, "TaskName1");
        Assert.assertNotNull(task1);
        Assert.assertEquals(Status.IN_PROGRESS, task1.getStatus());
        Assert.assertEquals("TaskName1", task1.getName());
    }

    @Test
    public void removeTaskById() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().removeTaskById(session, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals("TaskName1", task1.getName());
    }

    @Test
    public void startTaskByIndex() {
        @NotNull final Task task = bootstrap.getTaskEndpoint().startTaskByIndex(session, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void findTaskById() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().findTaskById(session, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals("TaskName1", task1.getName());
        Assert.assertEquals("TaskDescription1", task1.getDescription());
    }

    @Test
    public void finishTaskByName() {
        @NotNull final Task task = bootstrap.getTaskEndpoint().finishTaskByName(session, "TaskName1");
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().changeTaskStatusById(session, task.getId(), Status.IN_PROGRESS);
        Assert.assertNotNull(task1);
        Assert.assertEquals("TaskName1", task1.getName());
        Assert.assertEquals("TaskDescription1", task1.getDescription());
        Assert.assertEquals(Status.IN_PROGRESS, task1.getStatus());
    }

    @Test
    public void removeTaskByName() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().removeTaskByName(session, "TaskName1");
        Assert.assertNotNull(task1);
        Assert.assertEquals("TaskName1", task1.getName());
        Assert.assertEquals("TaskDescription1", task1.getDescription());
    }

    @Test
    public void finishTaskByIndex() {
        @NotNull final Task task = bootstrap.getTaskEndpoint().finishTaskByIndex(session, 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void changeTaskStatusByIndex() {
        @NotNull final Task taskNew = bootstrap.getTaskEndpoint().changeTaskStatusByIndex(session, 0, Status.NOT_STARTED);
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(Status.NOT_STARTED, taskNew.getStatus());
    }

    @Test
    public void findTaskAll() {
        @NotNull final List<Task> tasks = bootstrap.getTaskEndpoint().findTaskAll(session);
        Assert.assertNotNull(tasks);
    }

    @Test
    public void changeTaskStatusByName() {
        @NotNull final Task task1 = bootstrap.getTaskEndpoint().changeTaskStatusByName(session, "TaskName1", Status.COMPLETED);
        Assert.assertNotNull(task1);
        Assert.assertEquals("TaskName1", task1.getName());
        Assert.assertEquals("TaskDescription1", task1.getDescription());
        Assert.assertEquals(Status.COMPLETED, task1.getStatus());
    }

}
