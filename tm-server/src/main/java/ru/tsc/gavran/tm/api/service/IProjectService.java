package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IOwnerService<Project> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Comparator<Project> comparator);

    @NotNull
    Project findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project startById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project finishByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project finishByIndex(@Nullable String userId, @Nullable Integer index);

}