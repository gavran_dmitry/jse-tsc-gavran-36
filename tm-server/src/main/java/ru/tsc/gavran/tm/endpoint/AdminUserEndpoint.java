package ru.tsc.gavran.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.endpoint.IAdminUserEndpoint;
import ru.tsc.gavran.tm.api.service.IServiceLocator;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint() {
        super(null);
    }

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public User lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public User removeUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public User removeUserById(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "id", partName = "loginid") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeById(id);
    }

    @Override
    @Nullable
    @WebMethod
    public User unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public User setUserRole(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "role", partName = "role") @NotNull final Role role
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().setRole(session.getUserId(), role);
    }

    @Override
    @Nullable
    @WebMethod
    public User updateUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().updateUserById(session.getUserId(), firstName, lastName, middleName, email);
    }

    @Override
    @Nullable
    @WebMethod
    public User updateUserByLogin(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().updateUserByLogin(login, firstName, lastName, middleName, email);
    }

    @Override
    @Nullable
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @Override
    @Nullable
    @WebMethod
    public User getCurrentUser(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().getUser(session);
    }

    @Override
    @Nullable
    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Override
    @Nullable
    @WebMethod
    public User findUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "id", partName = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findById(id);
    }

    @NotNull
    @Override
    @WebMethod
    public void clearUser(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().clear();
    }

    @NotNull
    @Override
    @WebMethod
    public List<User> findAllUser(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

}