package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.ITaskRepository;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task findByName(@NotNull String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task removeByName(@NotNull String userId, @NotNull final String name) {
        @NotNull final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(this::remove);
        return task.orElseThrow(ProcessException::new);
    }

    @Nullable
    @Override
    public Task startById(@NotNull String userId, @NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task startByName(@NotNull String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task startByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final int index, @NotNull final Status status) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String id) {
        return findAll(userId).stream()
//                .filter(t -> t.getUserId().equals(userId))
                .filter(t -> t.getProjectId().equals(id))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Task bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        @NotNull final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    @Override
    public Task unbindTaskById(@NotNull String userId, @NotNull String id) {
        final Task task = findById(userId, id);
        task.setProjectId(null);
        return task;
    }

    @NotNull
    @Override
    public void unbindAllTaskByProjectId(@NotNull String userId, @NotNull String id) {
        findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(id))
                .filter(t -> t.getUserId().equals(userId))
                .forEach(t -> t.setProjectId(null));
    }

}