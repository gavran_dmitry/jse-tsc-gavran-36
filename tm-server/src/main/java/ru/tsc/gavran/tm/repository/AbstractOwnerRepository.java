package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IOwnerRepository;
import ru.tsc.gavran.tm.exception.empty.EmptyIdException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.AbstractOwnerEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @NotNull
    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        final List<E> listEntity = findAll(userId);
        list.remove(listEntity);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public void clear(@NotNull final String userId) {
        final List<String> listEnt = list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .map(E::getId)
                .collect(Collectors.toList());
        listEnt.forEach(list::remove);
    }

    @NotNull
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .filter(e -> e.getUserId().equals(userId))
                .findFirst()
                .orElseThrow(ProcessException::new);

    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final List<E> entity = findAll(userId);
        return entity.get(index);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return list.stream()
                .filter(f -> f.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(ProcessException::new);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Optional<E> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(this::remove);
        return task.orElseThrow(ProcessException::new);
    }

}