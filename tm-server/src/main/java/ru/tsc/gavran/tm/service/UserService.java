package ru.tsc.gavran.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IUserRepository;
import ru.tsc.gavran.tm.api.service.IPropertyService;
import ru.tsc.gavran.tm.api.service.IUserService;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.empty.*;
import ru.tsc.gavran.tm.exception.entity.UserEmailExistsException;
import ru.tsc.gavran.tm.exception.entity.UserLoginExistsException;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;


    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull IUserRepository userRepository, @NotNull IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeUserByLogin(login);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USER);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        if (isEmailExist(email)) throw new UserEmailExistsException(email);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USER);
        user.setFirstName("New");
        user.setMiddleName("User");
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (isLoginExist(login)) throw new UserLoginExistsException(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @NotNull
    @Override
    public User setRole(@Nullable String id, @Nullable Role role) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public boolean isLoginExist(@NotNull String login) {
        return userRepository.findByLogin(login) != null;
    }

    @NotNull
    @Override
    public boolean isEmailExist(@NotNull String email) {
        return userRepository.findByEmail(email) != null;
    }

    @NotNull
    @Override
    public User updateUserById(
            @Nullable final String id,
            @Nullable final String lastName,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String email
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyFullNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFullNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyFullNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExist(email)) throw new UserEmailExistsException(email);
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User updateUserByLogin(
            @Nullable final String login,
            @Nullable final String lastName,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyIdException();
        if (isLoginExist(email)) throw new UserLoginExistsException(login);
        if (lastName == null || lastName.isEmpty()) throw new EmptyFullNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFullNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyFullNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        return user;

    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        return user;
    }

}