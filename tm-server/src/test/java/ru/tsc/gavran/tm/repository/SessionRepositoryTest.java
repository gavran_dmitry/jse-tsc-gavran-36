package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.gavran.tm.model.Session;

import java.util.List;

public class SessionRepositoryTest {

    @Nullable
    private SessionRepository sessionRepository;

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @Before
    public void before() {
        sessionRepository = new SessionRepository();
        @NotNull final Session session = new Session();
        session.setUserId(TEST_USER_ID);
        this.session = sessionRepository.add(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals(TEST_USER_ID, session.getUserId());

        @NotNull final Session sessionById = sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session, sessionById);
    }

    @Test
    public void findById() {
        @NotNull final Session session = sessionRepository.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionRepository.findAll(this.session.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test
    public void remove() {
        sessionRepository.removeById(session.getId());
        Assert.assertNull(sessionRepository.findById(session.getId()));
    }

    @Test
    public void removeById() {
        sessionRepository.removeById(session.getId());
        Assert.assertNull(sessionRepository.findById(session.getId()));
    }

}