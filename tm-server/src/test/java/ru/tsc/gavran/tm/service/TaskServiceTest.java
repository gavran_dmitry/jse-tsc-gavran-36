package ru.tsc.gavran.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.repository.TaskRepository;


import java.util.List;

public class TaskServiceTest {

    @Nullable
    private static TaskService taskService;

    @Nullable
    private static Task task;

    @NotNull
    protected static final String TEST_TASK_NAME = "TaskTestName";

    @NotNull
    protected static final String TEST_DESCRIPTION = "TaskTestDescription";

    @NotNull
    protected static final String TEST_USER_ID = "TaskTestUserId";

    @Before
    public void before() {
       taskService = new TaskService(new TaskRepository());
       task = taskService.add(TEST_USER_ID , new Task(TEST_TASK_NAME, TEST_DESCRIPTION));
    }

    @Test
    public void findById() {
        @NotNull final Task tasks = taskService.findById(task.getId());
        Assert.assertEquals(TEST_TASK_NAME, tasks.getName());

    }
    @After
    public void after() {
        taskService.clear();
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void create() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());

        @NotNull final Task taskById = taskService.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task, taskById);
    }


    @Test
    public void changeStatusById() {
        @Nullable final Task task = taskService.changeStatusById(TEST_USER_ID, this.task.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Task task = taskService.changeStatusByName(TEST_USER_ID, TEST_TASK_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    };

    @Test
    public void changeStatusByIndex() {
        @Nullable final Task task = taskService.changeStatusByIndex(TEST_USER_ID, 0, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    };

    @Test
    public void findByName() {
        @Nullable final Task task = taskService.findByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(task);
    };

    @Test
    public void removeByName() {
        @Nullable final Task task = taskService.removeByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(task);
    };

    @Test
    public void removeByIndex() {
        @Nullable final Task task = taskService.removeByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(task);
    };

    @Test
    public void updateByIndex() {
        @Nullable final Task task = taskService.updateByIndex(TEST_USER_ID,0, TEST_TASK_NAME, TEST_DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getDescription(), TEST_DESCRIPTION);
    };

    @Test
    public void updateById() {
        @Nullable final Task task = taskService.updateById(TEST_USER_ID,this.task.getId(), TEST_TASK_NAME, TEST_DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getDescription(), TEST_DESCRIPTION);
    };

    @Test
    public void startById() {
        @Nullable final Task task = taskService.startById(TEST_USER_ID, this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    };

    @Test
    public void startByName() {
        @Nullable final Task task = taskService.startByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    };

    @Test
    public void startByIndex() {
        @Nullable final Task task = taskService.startByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    };

    @Test
    public void finishById() {
        @Nullable final Task task = taskService.finishById(TEST_USER_ID, this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    };

    @Test
    public void finishByName() {
        @Nullable final Task task = taskService.finishByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    };

    @Test
    public void finishByIndex() {
        @Nullable final Task task = taskService.finishByIndex(TEST_USER_ID, 0);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    };

}
